# DungeonBot
*Copyright LuzFaltex 2017*

### Commands
`[optional] <required>`

`~roll [number]d<sides>[+/-mod]`
    - Rolls [number] of virtual die with <sides> sides, adding or subtracting mod.

`~ping`
    - Pong!
    
`~help`
    - Shows information and stuffs