﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonBot
{
    public static class Globals
    {
        public enum CommandRoom : ulong
        {
            Testing = 370110289628102656,
            Production = 294597258895163402
        }

        private static bool Debug = false;

        public static ulong GetRoom()
        {
            return Debug ? (ulong)CommandRoom.Testing : (ulong)CommandRoom.Production;
        }
    }
}
