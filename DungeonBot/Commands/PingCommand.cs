﻿using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DungeonBot.Commands
{
    public class PingCommand : ModuleBase<SocketCommandContext>
    {
        [Command("ping")]
        [Summary("pong")]
        public async Task PingPongAsync()
        {
            if (Context.Channel.Id.Equals(Globals.GetRoom()))
            {
                // I accept no arguments
                await ReplyAsync("pong!");
            }
        }
    }
}
