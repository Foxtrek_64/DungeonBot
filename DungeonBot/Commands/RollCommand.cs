﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DungeonBot.Commands
{
     public class RollCommand : ModuleBase<SocketCommandContext>
     {
        // Roll dice
        [Command("roll")]
        [Summary("Rolls virtual dice.")]
        public async Task RollAsync([Summary("<Number>d<Sides>")][Remainder] string rollArg)
        {
            if (Context.Channel.Id.Equals(Globals.GetRoom()))
            {
                try
                {
                    int number, sides, mod;

                    if (!SplitInput(rollArg, out number, out sides, out mod))
                    {
                        throw new ArgumentException("InvalidArgumentException: Format must be <number>d<sides>");
                    }
                        
                    await Context.Message.DeleteAsync();
                    StringBuilder rollResponse = new StringBuilder();

                    List<int> rolls = new List<int>();

                    Random rnd = new Random();
                    for (int i = 0; i < number; i++)
                    {
                        rolls.Add(rnd.Next(1, sides + 1));
                    }

                    SocketGuildUser user = (SocketGuildUser)Context.User;
                    if (rolls.Count == 1)
                    {
                        rollResponse.Append($"{user.Nickname} rolled a d{sides}: {rolls[0] + mod}");
                        if (mod > 0)
                        {
                            rollResponse.AppendLine($" ({rolls[0]} + {mod})");
                            Console.WriteLine($"{rolls[0] + mod} ({rolls[0]} + {mod})");
                        }
                        else Console.WriteLine(rolls[0]);
                    }
                    else
                    {
                        
                        rollResponse.Append($"{user.Nickname} rolled a d{sides} (mod {mod}) {number} times: ");
                        string results = String.Join(", ", rolls.Select(x => (mod != 0 ? $"{x + mod} ({x}{(mod > 0 ? "+" : "-")}{Math.Abs(mod)})" : $"x")));
                        rollResponse.AppendLine(results);
                        Console.WriteLine(results);
                        rollResponse.AppendLine($"Total: {rolls.Take(rolls.Count).Sum()}");
                    }

                    await ReplyAsync(rollResponse.ToString());
                }
                catch (Exception ex)
                {
                    throw;
                }

            }
        }



        /// <summary>
        /// Takes in a string formatted as 1d20+5 and splits it into its corresponding parts
        /// </summary>
        /// <returns>Success</returns>
        private bool SplitInput(string input, out int number, out int sides, out int mod)
        {
            string re1 = "(\\d+)";  // input
            string re2 = "(d)"; // the 'd'
            string re3 = "(\\d+)";  // sides
            string re4 = "([-+]\\d+)";	// mod

            // Quick fixes because I don't know how to regex.
            if (input.First().Equals('d'))
            {
                input = "1" + input;
            }
            if (!input.Contains('+') && !input.Contains('-'))
            {
                input += "+0";
            }
            // Remove any spaces that may throw it off
            input.Replace(" ", "");
            
            // Enough hand-holding. Now we figure out if the user knows what they're doing.
            Regex r = new Regex(re1 + re2 + re3 + re4, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m = r.Match(input);
            if (m.Success)
            {
                number = int.Parse(m.Groups[1].ToString());
                sides = int.Parse(m.Groups[3].ToString());
                mod = int.Parse(m.Groups[4].ToString());
                Console.Write("(" + String.Join(") (", number, sides, mod) + ") : ");
            }
            else
            {
                number = 0;
                sides = 0;
                mod = 0;
            }

            return m.Success;
        }
    }
}
