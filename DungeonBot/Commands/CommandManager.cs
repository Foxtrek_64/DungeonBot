﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DungeonBot.Commands
{
    public class CommandManager
    {
        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;

        public async Task InstallCommandsAsync(DiscordSocketClient client, CommandService commands, IServiceProvider services)
        {
            _client = client;
            _commands = commands;
            _services = services;

            // Hook the MessageReceived Event into our Command Handler
            _client.MessageReceived += HandleCommandAsync;
            // Discover all of the commands in this assembly and load them
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Don't process the command if it was a System Message
            var message = messageParam as SocketUserMessage;
            if (message == null) return;

            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;

            // Determine if the message is a command based on if it starts with '~' or a mention prefix
            if (!(message.HasCharPrefix('~', ref argPos) || message.HasMentionPrefix(_client.CurrentUser, ref argPos))) return;

            //Create a command context
            var context = new SocketCommandContext(_client, message);            

            // Execute a the command.
            var result = await _commands.ExecuteAsync(context, argPos, _services);
            if (!result.IsSuccess)
            {
                Discord.Rest.RestUserMessage errorMessage;
                if (String.IsNullOrEmpty(result.ErrorReason))
                {
                    errorMessage = await context.Channel.SendMessageAsync(result.ErrorReason);
                }
                else
                {
                    // Catch error
                    EmbedBuilder embed = new EmbedBuilder();
                    embed.Color = Color.Red;
                    embed.Title = result.ErrorReason.ToString().Split(':')[0];
                    embed.Description = result.ErrorReason.Substring(embed.Title.Length + 1);
                    errorMessage = await context.Channel.SendMessageAsync("", embed: embed.Build());
                }
                
                await Task.Delay(1000 * 10);

                // After ten second delay, clean messages
                await context.Message.DeleteAsync();
                await errorMessage.DeleteAsync();
                
            }
        }
    }
}
