﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DungeonBot.Commands
{
    public class HelpCommand : ModuleBase<SocketCommandContext>
    {
        // Show help
        [Command("help")]
        [Summary("Displays command guide")]
        public async Task HelpAsync()
        {
            if (Context.Channel.Id.Equals(Globals.GetRoom()))
            {
                EmbedBuilder embed = new EmbedBuilder();
                StringBuilder sb = new StringBuilder();
                {
                    sb.AppendLine(" - roll - Rolls virtual dice");
                    sb.AppendLine(" -- [number to roll]d<number of sides>[+/-mod]");
                    sb.AppendLine(" -- `~roll 2d20+5`");
                    sb.AppendLine();
                    sb.AppendLine(" - help - Shows this dialog");
                    sb.AppendLine();
                    sb.AppendLine(" - ping - pong! (tests if the bot's responding)");
                }

                embed.Color = Color.Green;
                embed.ThumbnailUrl = "https://www.luzfaltex.com/media/7-sudo-icon-png/";
                embed.Title = $"Dungeon Bot \u00a9 {GetYear()} LuzFaltex";
                embed.Description = "D&D Utility bot built by Foxtrek_64";
                embed.AddField("Command Information", "Command prefix: `~`\r\nArgumentSyntax: [optional] <required>");
                embed.AddField("Commands", sb.ToString(), true);
                

                await ReplyAsync("", embed: embed.Build());
            }
        }

        private string GetYear()
        {
            return $"2017{((DateTime.Now.Year > 2017) ? $" - {DateTime.Now.Year}" : "" )}";
        }
    }
}
