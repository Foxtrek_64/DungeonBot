﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using DungeonBot.Commands;

namespace DungeonBot
{
    public class Program
    {
        public static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

        private CommandService _commands;
        private DiscordSocketClient _client;
        private IServiceProvider _services;

        private CommandManager commandManager;

        public async Task MainAsync()
        {
            _client = new DiscordSocketClient();
            _commands = new CommandService();

            _client.Log += Log;

            string token = "PRIVATE_BOT_TOKEN";

            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .BuildServiceProvider();

            commandManager = new CommandManager();

            await commandManager.InstallCommandsAsync(_client, _commands, _services);

            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();

            // Block until the program is closed
            await Task.Delay(-1);
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}
